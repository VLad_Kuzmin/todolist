import React, { Component } from "react";
import ToDoItems from "/.ToDoItems"

class ToDoList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: []
        };

        this.addItem = this.addItem.bind(this)
    }

    addItem(e) {

        var itemArray = this.state.items;

       /* unshifting method add new item to the beginning of array and return  text and key */
        if (this._inputElement.value !== "") {
        itemArray.unshift({
            text: this._inputElement.value,
            key: Date.now()
        });

        this.setState({
            items: itemArray
        });
        this._inputElement.value = "";
    }

    console.log(itemArray);
 /* convert placeholder to the beginning condition */
    e.preventDefault();
}
    render() {
        return <div className="ToDoListMain">
            <div className="header">
                <form onSubmit={this.addItem}>
                    <input ref={(a)=> this._inputElement = a}
                           placeholder="enter task"/>
                    <button type="submit"> add Task</button>
                </form>
            </div>
            /*Make elements visible */
             <ToDoItems entries={this.state.items}/>
        </div>;
    }
}
export default ToDoList;
